## 数联网介绍
随着互联网及新一代信息技术与应用的蓬勃发展，数据成为基础性战略资源和关键性生产要素。在开放、动态、难控的互联网之上实现可信可管可控的数据互联互通与融合应用，是国家和社会治理数字化转型以及数字经济健康发展的重大需求，是大数据技术体系从计算为中心向数据为中心转型的主线之一，蕴含着机器为中心的互联网衍生出数据为中心的新型基础设施的重大技术变革。

数联网是基于互联网的虚拟数据网络，通过开放式软件体系结构和标准化协议，高效连接各种数据平台和系统，支撑异构异域异主数据的互联互通互操作，形成“数据互联、应需调度、域内自主、域间协作”的数据空间。

![数联网](./_static/imgs/datanet.png)

## 数字对象架构

数字对象架构（Digital Object Architecture, DOA）是由图灵奖得主，互联网之父罗伯特·卡恩提出的一种以数据为中心的开放式软件体系结构。
DOA包括一个基本模型、两个基础协议和三个核心系统。DOA基于数字对象（Digital Object，DO）模型统一抽象互联网资源以屏蔽资源的异构性。
一个数字对象分为标识、元数据、数据实体（数据源）三个部分，其中标识是数字对象的身份ID，唯一且持久的识别每个数字对象；元数据是数据的描述信息，用于发现、搜索数字对象；数据实体（数据源）则代表原始数据。
数字对象的三个部分分别由数字对象标识系统、数字对象注册表系统、数字对象仓库系统进行管理，并通过两个标准协议：数字对象标识解析协议（Digital Object Identifier/Resolution protocol,DO-IRP）和数字对象接口协议（Digital Object Interface Protocol, DOIP）进行访问，解析、搜索、使用数字对象。

![](./_static/imgs/DOAArchitecture.png)

DOA已在数字图书馆领域取得了全球性的规模化应用，即DOI系统。通过将书籍、论文、报告、视频等数字资源构建为数字对象，并分配唯一且持久的DOI标识，可以在任意一个支持DOI的应用系统中解析到该标识对应的文献实体，避免了常见的URL失效导致的资源不可访问问题。截至2021年5月，DOI系统在全球已注册了约2.57亿数字对象，覆盖了IEEE、ACM、Springer以及万方、知网等众多国内外学术数据库。

## BDWare数瑞：基于数字对象架构的数联网基础设施软件
BDWare数瑞是由北京大学牵头研发的开源系统软件，采用DOA架构，可用于搭建数联网基础设施。
基于分散式的体系结构，在互联网上搭建若干数字对象标识系统节点、注册表系统节点、仓库系统节点，并基于标准协议使其相互连接，形成一个网络化的软件系统，
进而支撑数据以数字对象的形式在全域互联网上的标识、解析、搜索、发现和使用。

![](./_static/imgs/DOABasedIoD.png)

## 基于BDWare开源软件，搭建数联网基础设施

BDWare的基础系统软件包括：标识解析系统（Router），注册表系统（SearchEngine），仓库网关系统（Gateway）以及仓库系统示例（Repo）。系统之间的连接关系如下图所示。

![](./_static/imgs/BDWareArch.png)

在数联网中，所有的数字对象、系统、用户均有一个唯一的ID，由标识解析系统分配。
标识解析系统为层次化结构，当注册表、仓库、网关启动时，需要指定一个标识解析系统建立连接，获得由该标识解析系统分配的标识，并将自己的状态信息（服务地址等）提交给标识解析系统。

注册表系统负责管理数字对象的元数据并提供搜索服务。当仓库启动后，可以选择与某个或若干注册表系统建立连接以便创建DO时可以将元数据注册至注册表中，进而可以提供全网的发现。

仓库系统管理数字对象实体，也即真实的数据，负责将原始数据封装为数字对象，向标识解析系统申请标识以及向注册表系统注册元数据。
目前BDWare示例仓库支持将CSV文件，或一张关系型数据库表封装为一个数字对象。
用户可根据实际的业务需求，将业务场景下的业务对象，如停车管理系统中的车辆、场地，购物系统中的商品、订单等封装为数字对象。

网关系统可以看做是数字对象实体的访问代理服务器，对外行为等同于一个数字对象仓库。对内，其没有实际的存储存放数字对象实体，但可以接入并管理多个仓库系统。当接收到DOIP请求时，网关会将请求转发至对应的仓库系统中，获取响应结果并返回给请求方。

## BDWare生态体系
BDWare数瑞，秉承开源开放、共建共治的理念，希望与大家共同建设与发展数联网新型基础设施与产业生态。
<!-- ![](./_static/imgs/BDWareEcoLayer.png)-->
## 获取代码
该软件栈中北大数瑞开源部分均已托管在gitee平台。
其中，两个协议的参考实现的集合[doa-sdk-bundle](https://gitee.com/bdoa/doa-sdk-bundle)。
其他代码仓库说明如下。

| 类别    | 名称                                                                 | 说明                                     |
|-------|--------------------------------------------------------------------|----------------------------------------|
| 协议    | [doip-sdk](https://gitee.com/BDWare/doip-sdk)                      | DOIPV2.0与2.1的编解码器                      |
| 协议    | [irp-sdk](https://gitee.com/BDWare/irp-sdk)                        | IRP协议的编解码器                             |
| 协议    | [doa-audit-tool](https://gitee.com/BDWare/doip-audit-tool)         | 实现DOIP协议与IRP协议自动存证与审计的工具               |
| 运行环境  | [distributed-runtime](https://gitee.com/BDWare/bdcontract-bundle)  | 数联网基础构件的运行支撑环境，支撑标识解析、数字对象仓库等软件的分布自治运行 |
| 基础构件  | [标识解析系统](https://gitee.com/BDWare/datanet-router-bundle)           | 标识网络的基础构件，支持自定义标识                      |
| 基础构件  | [数字对象注册表系统](https://gitee.com/BDWare/datanet-search-engine-bundle) | 注册表网络的基础构件，基于DOIP协议提供服务，支持元数据按需组网同步    |
| 基础构件  | [数字对象仓库系统示例](https://gitee.com/BDWare/datanet-repo-bundle)         | 基于DOIP协议，支持库表、文件等数据类型转化为数字对象           |
| 基础构件  | [数字对象仓库网关](https://gitee.com/BDWare/datanet-gateway-bundle)        | 基于DOIP协议，支持对多个数字对象仓库的统一访问控制和协议转发       |
| 测试工具  | [DOIP协议测试工具](https://gitee.com/BDWare/doip-testsuite)              | DOIP协议的测试工具，用于测试目标仓库、注册表的功能和性能         |
| 兼容性工具 | [DOIP兼容性工具](https://gitee.com/BDWare/doip-compatibility-tool)      | DOIP兼容性解决方案，支撑DOIP v2.0和v2.1的兼容互通      |

## 获取预配置的BDWare基础设施Docker镜像

我们已将预配置的BDWare基础设施镜像上传至Docker镜像仓库，包含完整的BDWare基础设施系统。
参考[部署说明](https://gitee.com/BDWare/iod-docker-deploy)，下载镜像并运行BDWare。

系统运行示意可参考[演示视频](./_static/videos/BDWareDockerIntro.mp4)。

## 扩展阅读

| 类别   | 名称                                                                                                    | 说明           |
|------|-------------------------------------------------------------------------------------------------------|--------------|
| 扩展阅读 | [数联网简介](https://dl.ccf.org.cn/article/articleDetail.html?type=xhtx_thesis&_ack=1&id=5743376176760832) | 数联网概念介绍      |
| 扩展阅读 | [数联网技术白皮书](https://gitee.com/BDWare/BDWare/raw/master/_static/files/whitepaper_1.0.pdf)               | 数联网技术整体介绍    |
| 协议标准 | [DOIP协议 v2.1](http://doa-atsd.org/task.group.2/DOIP.recommendation.2022-2-24.pdf)                     | DOIP新版标准协议规范 |
| 协议标准 | [IRP协议](https://www.dona.net/sites/default/files/2022-06/DO-IRPV3.0--2022-06-30.pdf)                  | IRP协议规范      |                                                                         



## 联系我们
邮箱：bdware@pku.edu.cn 

Copyright © 2018-2022 北大数瑞保留所有版权
